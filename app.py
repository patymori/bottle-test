# coding: utf-8
import os
import json

from bottle import Bottle, run, static_file, request, response


app = Bottle()


@app.route('/hello', method='GET')
def hello():
    return "Hello World!"


@app.route('/Shared/upload', method='POST')
def upload_file():
    upload = request.files.get('upload')
    upload_path = os.path.join(os.path.dirname(__file__), "upload")
    upload.save(upload_path)
    return 'OK'


@app.route('/Shared', method='GET')
@app.route('/Shared/<filepath:path>', method='GET')
def shared_files(filepath=None):
    upload_path = os.path.join(os.path.dirname(__file__), "upload")
    if filepath:
        print("filepath: %s" % filepath)
        if os.path.isfile(os.path.join(upload_path, filepath)):
            print("upload_path %s is a file" % filepath)
            return static_file(filepath, root=upload_path)
        upload_path = os.path.join(upload_path, filepath)

    print("upload_path %s is dir" % upload_path)
    response.content_type = "application/json"
    file_list = os.listdir(upload_path)
    return json.dumps(file_list)
